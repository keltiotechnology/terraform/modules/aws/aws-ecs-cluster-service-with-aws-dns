# Environment settings
#################################################
variable "region" {
  type        = string
  description = "AWS region"
  default     = "eu-central-1"
}

variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp'"
  default     = "swappy"
}

variable "stage" {
  type        = string
  description = "Stage, which could be 'test' 'dev' 'prod' ..."
  default     = "dev"
}


# Input for App
#################################################
variable "app_fqdn" {
  type        = string
  description = "App FQDN"
}

variable "app_name" {
  type        = string
  description = "Name of the application"
}

variable "app_image_name" {
  type        = string
  description = "The docker image name of the application"
}

variable "app_image_tag" {
  type        = string
  description = "The docker image tag of the application"
}

variable "app_container_name" {
  type        = string
  description = "The container name in AWS ECS"
}

variable "app_host_port" {
  type        = string
  description = "The port the expose the app"
}

variable "app_container_port" {
  type        = string
  description = "The port exposed by the app"
}

variable "app_container_cpu" {
  type        = string
  description = "The port exposed by the app"
}

variable "app_container_memory" {
  type        = string
  description = "The port exposed by the app"
}

variable "app_reserved_cpu" {
  type        = string
  description = "The port exposed by the app"
}

variable "app_reserved_memory" {
  type        = string
  description = "The port exposed by the app"
}

variable "app_container_command" {
  type        = list(string)
  description = "The port exposed by the app"
}

variable "app_desired_count" {
  type        = string
  description = "The port exposed by the app"
}

variable "app_heathcheck_path" {
  type        = string
  description = "Heathcheck path for application service"
}

variable "app_env_vars" {
  type        = map(string)
  description = "Environnement variables exposed in container"
  default     = {}
}

variable "app_env_secrets" {
  type        = map(string)
  description = "Secrets exposed as environnement variables in container"
  default     = {}
}

variable "app_target_group" {
  type        = string
  description = "App load balancer target group"
}

variable "app_launch_type" {
  type        = string
  description = "App ecs requires compatibilites : Set of launch types required by the task. The valid values are FARGATE or EC2"
  default     = "FARGATE"
}

variable "app_network_mode" {
  type        = string
  description = "App network mode : The networking behavior of Amazon ECS tasks hosted on Amazon EC2 instances is dependent on the network mode defined in the task definition.. The valid values are awsvpc when app_launch_type is FARGATE and bridge when app_launch_type is EC2"
  default     = "awsvpc"
}

variable "ecs_task_family" {
  type        = string
  description = "Ecs task family name to create"
  default     = "FARGATE"
}

variable "cloudfront_distribution" {
  type        = string
  description = "Cloudfront distribution used to render the service"
}

# Input for IAM
#################################################
variable "iam_ecs_instance_role_name" {
  type        = string
  description = "Environnement variables exposed in container"
  default     = "ecsInstanceRole"
}

variable "iam_ecs_task_execution_role_name" {
  type        = string
  description = "Environnement variables exposed in container"
  default     = "ecsTaskExecutionRole"
}


# Input for Module VPC
#################################################
variable "vpc_name" {
  type        = string
  description = "VPC used for the ECS cluster"
  default     = "vpc"
}

variable "ecs_cluster_name" {
  type        = string
  description = "ECS cluster name of the app"
  default     = "ecs"
}

variable "application_load_balancer_name" {
  type        = string
  description = "Load balancer name for the app"
  default     = "alb"
}

variable "alb_priority" {
  type        = number
  description = "Load balancer priority"
  default     = 100
}

variable "security_groups" {
  type        = list(string)
  description = "Security groups ids used for ecs service tasks"
}

variable "subnets" {
  type        = list(string)
  description = "Subnets ids where the ecs service will run"
  default     = []
}

# Input for Monitoring
#################################################
variable "cloudwatch_log_group_name" {
  type        = string
  description = "Name of the Cloudwatch log group for ecs"
  default     = "ecs-logs"
}

variable "cloudwatch_log_retention_in_days" {
  type        = number
  description = "Cloudwatch log retention for the service (in days)"
  default     = 30
}

# DNS
#################################################
variable "hosted_zone_id" {
  type        = string
  description = "Aws dns hosted zone id"
}
